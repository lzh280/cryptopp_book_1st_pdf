# cryptopp_book_1st_pdf

#### 介绍
《深入浅出CryptoPP密码学库》随书源代码（PDF版本）

CPP版随书源代码：https://gitee.com/locomotive_crypto/book_code

CryptoPP(Crypto++)官网: https://www.cryptopp.com/

#### 作者

韩露露 杨波

#### 封面
![书籍封面](https://gitee.com/locomotive_crypto/book_code/raw/master/photo/photo2.png )

#### 目录

第1章 绪论

第2章 安装和配置CryptoPP库

第3章 程序设计基础

第4章 初识CryptoPP库

第5章 随机数发生器

第6章 Hash函数

第7章 流密码

第8章 分组密码

第9章 消息认证码

第10章 密钥派生和基于口令的密钥派生

第11章 公钥密码数学基础

第12章 公钥加密

第13章 数字签名

第14章 密钥协商

第15章 建立安全信道

附录

#### 购买链接

京东：https://item.jd.com/12683807.html

当当：http://product.dangdang.com/28980749.html

#### 联系方式

locomotive_crypto@163.com

#### 声明
引用请注明出处：https://gitee.com/locomotive_crypto
